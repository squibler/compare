<?php
namespace DS\Validators\CreditCardRules;

use DS\Validators\CardValidator;

class MasterCard extends CardValidator
{
    protected $beginsWithRegex = '/^5[1|2|4|5|6]/';
    protected $validLength = 16;
}
