<?php

require '../vendor/autoload.php';

use DS\Builders\CreditCardBuilder;

$cards = [
    4111111111111111,
    4111111111111,
    4012888888881881,
    378282246310005,
    6011111111111117,
    5105105105105100,
    '5105 1051 0510 5106',
    9111111111111111
];


$creditCard = CreditCardBuilder::instance();

foreach ($cards as $card) {
    if ($validated = $creditCard->number($card)->validate()) {
        echo sprintf('%s: %s Valid<br/>', $validated['type'], $validated['number']);
    } else {
        echo sprintf('UNKNOWN: %s Invalid<br/>', $card);
    }
}
