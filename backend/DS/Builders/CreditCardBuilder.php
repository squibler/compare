<?php

namespace DS\Builders;

use DS\CreditCard;
use DS\Validators\CreditCardRules\Visa;
use DS\Validators\CreditCardRules\MasterCard;
use DS\Validators\CreditCardRules\Amex;
use DS\Validators\CreditCardRules\Discover;

class CreditCardBuilder
{
    public static function instance()
    {
        $cc = new CreditCard();
        $cc->addValidator(new Visa)
            ->addValidator(new MasterCard)
            ->addValidator(new Amex)
            ->addValidator(new Discover);
        return $cc;
    }
}
