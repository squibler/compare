<?php
namespace DS\Validators;

use DS\CreditCard;

abstract class CardValidator
{
    protected $card;


    public function __construct()
    {
        // Must have the child properties defined
        if (!$this->beginsWithRegex || !$this->validLength) {
            throw new Exception('You must define $beginsWithRegex and $validLength in ' . get_class($this));
        }
    }


    public function validate(CreditCard $card)
    {
        try {
            $this->isValidNumber($card);
            $this->isValidLength($card);
            $this->isValidLuhn($card);

            $length = strlen((string) $card->number());
        } catch (\Exception $e) {
            // Log it
            return false;
        }

        return [
            'type' => $this->name ?? (new \ReflectionClass($this))->getShortName(),
            'number' => substr_replace($card->number(), str_repeat('X', $length-6), 2, $length - 6)
        ];
    }


    protected function isValidNumber(CreditCard $card)
    {
        $number = $card->number();
        if (!preg_match($this->beginsWithRegex, $number)) {
            throw new \Exception('Visa card number must begin with 4');
        }
    }


    protected function isValidLength(CreditCard $card)
    {
        if (!is_array($this->validLength)) {
            $this->validLength = [ $this->validLength ];
        }

        $length = strlen((string) $card->number());
        if (!in_array($length, $this->validLength)) {
            throw new \Exception('Visa card number must be 13 or 16 digits in length');
        }
    }


    protected function isValidLuhn(CreditCard $card)
    {
        $number = $card->number();
        $sum = 0;
        $numDigits = strlen($number);
        $parity = $numDigits % 2;
        for ($i = 0; $i < $numDigits; $i++) {
            $digit = substr($number, $i, 1);
            if ($parity === ($i % 2)) {
                $digit <<= 1;
                if (9 < $digit) {
                    $digit = $digit - 9;
                }
            }
            $sum += $digit;
        }
        if (0 !== ($sum % 10)) {
            throw new \Exception('Credit Card number does not pass luhn algorithm');
        }
    }
}
