<?php
namespace DS\Validators\CreditCardRules;

use DS\Validators\CardValidator;

class Discover extends CardValidator
{
    protected $beginsWithRegex = '/^16|6011/';
    protected $validLength = 16;
}
