<?php
namespace DS\Validators\CreditCardRules;

use DS\Validators\CardValidator;

class Amex extends CardValidator
{
    protected $name = 'AMEX';
    protected $beginsWithRegex = '/^37/';
    protected $validLength = 15;
}
