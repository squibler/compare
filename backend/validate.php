<?php
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization, X-Request-With');

require '../vendor/autoload.php';

use DS\Builders\CreditCardBuilder;

$creditCard = CreditCardBuilder::instance();

$body = json_decode(file_get_contents('php://input'));


echo json_encode($creditCard->number($body->number)->validate());
