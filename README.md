# Daniel Smith Assessment
Please take a look at backend/DS for the main code and backend/index.php for the
basic usage.

## Set up
```
$ composer install
$ npm i
```

I only use composer for the autoloading no other libraries.
With the frontend I'm using a very simple vuejs app with axios for making requests to the backend.

## Backend
```
$ cd /path/to/docroot/backend
$ php -S localhost:8000
```

Visit http://localhost:8000 and you you should see all the validated cards

## Frontend
> NB. Leave the Backend running for the frontend to run correctly
```
$ cd /path/to/docroot
$ npm run serve
```
It will show a localhost url to visit in the browser.
