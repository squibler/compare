<?php
namespace DS;

use DS\Validators\CardValidator;

class CreditCard
{
    private $number;
    private $validators = [];

    public function __construct($cardNumber = null)
    {
        if ($cardNumber) {
            $this->setNumber($cardNumber);
        }
    }


    public function number($cardNumber = null)
    {
        if (!isset($cardNumber)) {
            return $this->number;
        }

        if (is_string($cardNumber)) {
            $cardNumber = (int) preg_replace('/[^\d]/', '', $cardNumber);
        }

        if (!is_int($cardNumber)) {
            throw new Exception('Expecting the card number to be a number!');
        }

        $this->number = $cardNumber;
        return $this;
    }


    public function addValidator(CardValidator $validator)
    {
        array_push($this->validators, $validator);
        return $this;
    }


    public function validate()
    {
        if (!$this->number) {
            return false;
        }

        foreach ($this->validators as $validator) {
            if ($isValid = $validator->validate($this)) {
                return $isValid;
            }
        }

        return false;
    }
}
