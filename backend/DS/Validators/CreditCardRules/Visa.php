<?php
namespace DS\Validators\CreditCardRules;

use DS\Validators\CardValidator;

class Visa extends CardValidator
{
    protected $name = 'VISA';
    protected $beginsWithRegex = '/^4/';
    protected $validLength = [ 13, 16 ];
}
